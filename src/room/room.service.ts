import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Room } from './entities/room.entity';

@Injectable()
export class RoomService {
  constructor(
    @InjectRepository(Room)
    private roomRepository: Repository<Room>,
  ) {}

  findAll(): Promise<Room[]> {
    return this.roomRepository.find();
  }

  // async findAllByDormitoryId(dormitoryId: number): Promise<Room[]> {
  //   const rooms = await this.roomRepository
  //     .createQueryBuilder('room')
  //     .leftJoinAndSelect('room.students', 'students')
  //     .leftJoinAndSelect('room.items', 'items')
  //     .where('room.dormitoryId = :dormitoryId', { dormitoryId })
  //     .getMany();
  //   if (!rooms || rooms.length === 0) {
  //     throw new NotFoundException(
  //       `Rooms with dormitory ID ${dormitoryId} not found`,
  //     );
  //   }
  //   return rooms;
  // }

  async findAllByDormitoryId(
    dormitoryId: number,
    page: number,
    keyword: string,
  ): Promise<{ rooms: Room[]; totalPages: number }> {
    const limit = 10;
    const offset = (page - 1) * limit;

    const baseQuery = this.roomRepository
      .createQueryBuilder('room')
      .leftJoinAndSelect('room.students', 'students')
      .leftJoinAndSelect('room.items', 'items')
      .where('room.dormitoryId = :dormitoryId', { dormitoryId });

    if (keyword) {
      baseQuery.andWhere('(room.number LIKE :keyword)', {
        keyword: `%${keyword}%`,
      });
    }

    // Count total rooms
    const totalRooms = await baseQuery.getCount();

    // Calculate total pages
    const totalPages = Math.ceil(totalRooms / limit);

    // Get the rooms for the current page
    const rooms = await baseQuery
      .orderBy('room.number', 'ASC') // You can change the sorting as per your requirement
      .skip(offset)
      .take(limit)
      .getMany();

    return { rooms, totalPages };
  }

  async findOneByIdOne(id: number): Promise<Room | null> {
    // Find the room by its ID along with its associated teachers
    const room = await this.roomRepository
      .createQueryBuilder('room')
      .leftJoinAndSelect('room.students', 'students')
      .leftJoinAndSelect('room.items', 'items')
      .where('room.id = :id', { id })
      .getOne();
    if (!room) {
      throw new NotFoundException(`Room with ID ${id} not found`);
    }
    return room;
  }
}
