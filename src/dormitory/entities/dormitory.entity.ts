import { Affiliated_school } from 'src/affiliated_school/entities/affiliated_school.entity';
import { Room } from 'src/room/entities/room.entity';
import { Teacher } from 'src/teacher/entities/teacher.entity';
import { Request } from 'src/request/entities/request.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Information } from 'src/information/entities/information.entity';
import { Offer } from 'src/offer/entities/offer.entity';
import { REGISTRATION_STATUSES } from 'src/utils/constants';

@Entity({ name: 'dormitories' })
export class Dormitory {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column()
  name: string;

  @Column()
  address: string;

  @Column()
  floor: number;

  @Column()
  capacity: number;

  @Column({ type: 'bigint' })
  payment: number;

  @Column()
  image_url: string;

  @Column({
    type: 'enum',
    enum: REGISTRATION_STATUSES,
    default: REGISTRATION_STATUSES.DEFAULT,
  })
  registration_status: REGISTRATION_STATUSES;

  @OneToMany(
    () => Affiliated_school,
    (affiliated_school) => affiliated_school.dormitory,
  )
  affiliated_schools: Affiliated_school[];

  @OneToMany(() => Request, (request) => request.dormitory)
  requests: Request[];

  @OneToMany(() => Room, (room) => room.dormitory)
  rooms: Room[];

  @OneToMany(() => Teacher, (teacher) => teacher.dormitory)
  teachers: Teacher[];

  @OneToMany(() => Information, (information) => information.dormitory)
  informations: Information[];

  @OneToMany(() => Offer, (offer) => offer.dormitory)
  offers: Offer[];

  constructor(partial: Partial<Dormitory>) {
    Object.assign(this, partial);
  }
}
