import { Controller, Get, Param } from '@nestjs/common';
import { DormitoryService } from './dormitory.service';
import { Dormitory } from './entities/dormitory.entity';
import { StudentAccess, TeacherAccess } from 'src/decorators/guard.decorator';

@Controller('dormitories')
export class DormitoryController {
  constructor(private readonly dormitoryService: DormitoryService) {}

  @StudentAccess()
  @TeacherAccess()
  @Get()
  async findAll(): Promise<Dormitory[]> {
    return this.dormitoryService.findAll();
  }

  @StudentAccess()
  @TeacherAccess()
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Dormitory | null> {
    // return this.dormitoryService.findOneByIdWithTeachers(parseInt(id, 10));
    return this.dormitoryService.findOneByIdWithTeachers(parseInt(id, 10));
  }
}
