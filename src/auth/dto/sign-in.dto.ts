import { IsNotEmpty } from 'class-validator';

import { USER_TYPES } from './../../utils/constants';

export class SignInDto {
  @IsNotEmpty({ message: 'Please enter username' })
  email: string;

  @IsNotEmpty({ message: 'Please enter password' })
  password: string;

  @IsNotEmpty({ message: 'Please enter user type' })
  type: USER_TYPES;
}
