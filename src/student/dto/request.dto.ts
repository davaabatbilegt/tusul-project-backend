import { IsNotEmpty } from 'class-validator';
import { STATUSES } from 'src/utils/constants';

export class CreateRequestDto {
  email: string;
  phone_number: number;
  @IsNotEmpty({ message: 'Please enter gender' })
  gender: STATUSES;
  @IsNotEmpty({ message: 'Please enter gpa' })
  gpa: number;
  @IsNotEmpty({ message: 'Please enter status' })
  status: string;
  @IsNotEmpty({ message: 'Please enter gpa_desc' })
  gpa_desc: string;
  additional_desc: string;
  @IsNotEmpty({ message: 'Please enter student_id' })
  student_id: number;
  @IsNotEmpty({ message: 'Please enter dormitory_id' })
  dormitory_id: number;
}
