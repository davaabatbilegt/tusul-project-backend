import { Dormitory } from 'src/dormitory/entities/dormitory.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'affiliated_schools' })
export class Affiliated_school {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column()
  affiliated_school: string;

  @Column()
  score: number;

  @Column()
  dormitoryId: number;

  @ManyToOne(() => Dormitory, (dormitory) => dormitory.affiliated_schools)
  @JoinColumn({ name: 'dormitoryId' })
  dormitory: Dormitory;

  constructor(partial: Partial<Affiliated_school>) {
    Object.assign(this, partial);
  }
}
