import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Body,
} from '@nestjs/common';
import { ReminderService } from './reminder.service';
import { Reminder } from './entities/reminder.entity';
import { StudentAccess, TeacherAccess } from 'src/decorators/guard.decorator';

@Controller('reminders')
export class ReminderController {
  constructor(private readonly reminderService: ReminderService) {}

  // @StudentAccess()
  // @TeacherAccess()
  // @Get()
  // async getAllReminders(
  //   @Query('teacherId') teacherId?: number,
  //   @Query('studentId') studentId?: number,
  // ): Promise<Reminder[]> {
  //   return await this.reminderService.getAll(studentId, teacherId);
  // }

  @StudentAccess()
  @TeacherAccess()
  @Get('/student/:studentId')
  async getAllOffersByStudent(
    @Param('studentId') studentId: number,
  ): Promise<Reminder[]> {
    return await this.reminderService.getAllByStudent(studentId);
  }

  @StudentAccess()
  @TeacherAccess()
  @Get('/teacher/:teacherId')
  async getAllOffersByDormitory(
    @Param('teacherId') teacherId: number,
  ): Promise<Reminder[]> {
    return await this.reminderService.getAllByTeacher(teacherId);
  }

  @StudentAccess()
  @TeacherAccess()
  @Get(':id')
  async getReminderById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<Reminder | undefined> {
    return await this.reminderService.getOneById(id);
  }

  @TeacherAccess()
  @Post()
  async createReminder(
    @Body() reminderData: Partial<Reminder>,
  ): Promise<Reminder> {
    return await this.reminderService.create(reminderData);
  }
}
