import { Student } from 'src/student/entities/student.entity';
import { Teacher } from 'src/teacher/entities/teacher.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'reminders' })
export class Reminder {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column()
  description: string;

  @Column()
  image_url: string;

  @Column()
  minus_points: number;

  @Column()
  created_at: Date;

  @Column()
  studentId: number;

  @Column()
  teacherId: number;

  @ManyToOne(() => Student, (student) => student.reminders)
  @JoinColumn({ name: 'studentId' })
  student: Student;

  @ManyToOne(() => Teacher, (teacher) => teacher.reminders)
  @JoinColumn({ name: 'teacherId' })
  teacher: Teacher;

  constructor(partial: Partial<Reminder>) {
    Object.assign(this, partial);
  }
}
