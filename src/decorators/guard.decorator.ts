import { SetMetadata } from '@nestjs/common';

import { IS_PUBLIC_KEY, USER_TYPES } from '../utils/constants';

export const Public = () => SetMetadata(IS_PUBLIC_KEY, true);

export const AdminAccess = () => SetMetadata(USER_TYPES.ADMIN, true);

export const StudentAccess = () => SetMetadata(USER_TYPES.STUDENT, true);

export const TeacherAccess = () => SetMetadata(USER_TYPES.TEACHER, true);
