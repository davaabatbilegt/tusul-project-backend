import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from './entities/request.entity';
import { CreateRequestDto } from './dto/request.dto';
import { StudentService } from 'src/student/student.service';
import { Affiliated_schoolService } from 'src/affiliated_school/affiliated_school.service';
import { UpdateRequestDto } from './dto/update_request.dto';
import { REGISTRATION_STATUSES, REQUEST_STATUSES } from 'src/utils/constants';
import { DormitoryService } from 'src/dormitory/dormitory.service';

@Injectable()
export class RequestService {
  constructor(
    @InjectRepository(Request)
    private requestRepository: Repository<Request>,
    private studentService: StudentService,
    private schoolService: Affiliated_schoolService,
    private dormitoryService: DormitoryService,
  ) {}

  async findAllWithStudents(): Promise<Request[]> {
    const requests = await this.requestRepository.find({
      relations: ['student'],
    });
    requests.sort((a, b) => b.score - a.score);
    return requests;
  }

  // async findAllByDormitoryId(dormitoryId: number): Promise<Request[]> {
  //   const requests = await this.requestRepository.find({
  //     where: {
  //       dormitoryId: dormitoryId,
  //     },
  //     relations: ['student'], // Assuming 'students' is the correct property name for the relation
  //   });
  //   if (!requests || requests.length === 0) {
  //     throw new NotFoundException(
  //       `Requests with dormitory ID ${dormitoryId} not found`,
  //     );
  //   }
  //   // Sorting requests by score (descending order)
  //   requests.sort((a, b) => b.score - a.score);

  //   return requests;
  // }
  async findAllByDormitoryId(
    dormitoryId: number,
    page: number,
    keyword: string,
  ): Promise<{ requests: Request[]; totalPages: number }> {
    const limit = 10;
    const offset = (page - 1) * limit;

    // Initial query to count total matching requests
    let countQuery = this.requestRepository
      .createQueryBuilder('request')
      .where('request.dormitoryId = :dormitoryId', { dormitoryId });

    if (keyword) {
      countQuery = countQuery.andWhere('(request.email LIKE :keyword)', {
        keyword: `%${keyword}%`,
      });
    }

    const totalRequests = await countQuery.getCount();
    const totalPages = Math.ceil(totalRequests / limit);

    // Query to fetch the actual requests
    let query = this.requestRepository
      .createQueryBuilder('request')
      .where('request.dormitoryId = :dormitoryId', { dormitoryId })
      .leftJoinAndSelect('request.student', 'student');

    if (keyword) {
      query = query.andWhere('(request.email LIKE :keyword)', {
        keyword: `%${keyword}%`,
      });
    }

    const requests = await query
      .orderBy('request.score', 'DESC')
      .skip(offset)
      .take(limit)
      .getMany();

    return { requests, totalPages };
  }

  findOneById(id: number): Promise<Request | null> {
    return this.requestRepository.findOne({
      where: { id },
      relations: ['student'],
    });
  }

  async create(createRequestDto: CreateRequestDto): Promise<Request> {
    // const studentData = await this.studentService.findOneById(
    //   createRequestDto.studentId,
    // );
    // const schoolData = await this.schoolService.findOneBySchoolDormitory(
    //   studentData.school,
    //   createRequestDto.dormitoryId,
    // );
    // const score = this.solveScore(studentData.year, schoolData.score);
    const request = this.requestRepository.create({
      ...createRequestDto,
      score: 70,
    });
    return this.requestRepository.save(request);
  }

  solveScore(year: number, schoolScore: number) {
    let score = schoolScore;
    switch (year) {
      case 1:
        score = score + 50;
        break;
      case 2:
        score = score + 40;
        break;
      case 3:
        score = score + 30;
        break;
      case 4:
        score = score + 30;
        break;
      default:
        score = score + 20;
        break;
    }
    return score;
  }

  async findByIds(userId: number, dormitoryId: number): Promise<Request> {
    return await this.requestRepository.findOne({
      where: {
        studentId: userId,
        dormitoryId: dormitoryId,
      },
    });
  }

  async updateStatusById(
    id: number,
    updateRequestDto: UpdateRequestDto,
  ): Promise<Request | null> {
    const request = await this.requestRepository.findOneById(id);
    if (updateRequestDto.status) {
      request.status = updateRequestDto.status;
    }
    let score =
      Number(request.score) + (Number(updateRequestDto.add_score) || 0); // Convert to number before addition
    if (updateRequestDto.is_gpa && updateRequestDto.is_gpa === true) {
      score += Number(request.gpa) * 3; // Convert to number before addition
    }
    request.score = score;
    return await this.requestRepository.save(request);
  }

  async calculate(dormitory_id: number) {
    const limit = 50;
    const requests = await this.requestRepository.find({
      where: {
        dormitoryId: dormitory_id,
      },
      relations: ['student'],
    });
    requests.sort((a, b) => b.score - a.score);
    if (requests.length >= limit + 1) {
      const limitScore = requests[limit].score;
      requests.forEach((item) => {
        console.log(item);
        if (item.score > limitScore + 10) {
          item.status = REQUEST_STATUSES.APPROVE;
        } else if (item.score < limitScore - 10) {
          item.status = REQUEST_STATUSES.REFUSE;
        } else {
          item.status = REQUEST_STATUSES.CALCULATE;
        }
      });
      this.dormitoryService.updateRegistrationStatus(
        dormitory_id,
        REGISTRATION_STATUSES.CALCULATED,
      );
      await this.requestRepository.save(requests);
    } else {
      console.log('Not enough requests to perform calculations');
    }
  }

  async returnAnswer(dormitory_id: number) {
    const limit = 50;
    const requests = await this.requestRepository.find({
      where: {
        dormitoryId: dormitory_id,
      },
      relations: ['student'],
    });
    requests.sort((a, b) => b.score - a.score);
    if (requests.length >= limit + 1) {
      const limitScore = requests[limit].score;
      requests.forEach((item) => {
        if (item.score > limitScore) {
          this.studentService.updateStudent(item.studentId, {
            dormitoryId: item.dormitoryId,
          });
          item.status = REQUEST_STATUSES.APPROVE;
        } else {
          item.status = REQUEST_STATUSES.REFUSE;
        }
      });
      this.dormitoryService.updateRegistrationStatus(
        dormitory_id,
        REGISTRATION_STATUSES.RETURNED,
      );
      await this.requestRepository.save(requests);
    } else {
      console.log('Not enough requests to perform calculations');
    }
  }

  async delete(id: number): Promise<void> {
    const request = await this.requestRepository.findOneById(id);
    if (!request) {
      throw new NotFoundException(`Information with ID ${id} not found`);
    }
    await this.requestRepository.delete(id);
  }
}
